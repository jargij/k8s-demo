<?php
echo "\n";
echo '<h1>Hallo ik maak load...</h1><br>';
echo 'Node naam: ' . getenv('NODE_NAME').'<br>';
echo 'Host naam: ' . gethostname().'<br>';

$time_start = microtime(true);
$a = 0;

for ($i = 0; $i < 100000000; $i++) {
    $a += $i;
}

$time_end = microtime(true);
//dividing with 60 will give the execution time in minutes otherwise seconds
$execution_time = ($time_end - $time_start);

//execution time of the script
echo '<br> Exec. time:' . number_format($execution_time, 2) . ' sec<br>';
echo "\n";
