FROM php:7.3

RUN mkdir /app
COPY index.php /app

EXPOSE 8080

CMD ["php", "-S", "0.0.0.0:8080", "-t", "/app", "/app/index.php"]